import java.io.*;
import java.net.*;
import java.util.Scanner;

public class ClienteA {
    public static void main(String[] args) {
        try {
            Socket s = new Socket("localhost", 8080);

            // Solicitar al usuario que ingrese el valor de la compra
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ingrese el valor de la compra:");
            String valorCompra = scanner.nextLine();

            // Enviar el valor de compra
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println(valorCompra);

            // Recibir la respuesta
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String respuesta = in.readLine();
            System.out.println("El 7% del valor de la compra es: " + respuesta);

            // Cerrar la conexión
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
