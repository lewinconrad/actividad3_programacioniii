import java.io.*;
import java.net.*;

public class ServidorB {
    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(8080);
            System.out.println("Esperando conexión...");

            // Aceptar una conexión
            Socket s = ss.accept();
            System.out.println("Conexión establecida");

            // Recibir el valor de compra
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String valorCompra = in.readLine();

            // Calcular el 7% del valor de compra
            double valor = Double.parseDouble(valorCompra);
            double calculo = valor * 0.07;

            // Formatear el cálculo a dos decimales
            String calculoFormateado = String.format("%.2f", calculo) + "$";

            // Enviar la respuesta
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println(calculoFormateado);

            // Cerrar la conexión
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}